#include <iostream>
#define MSG "Ola Leandro"

using namespace std;

int main()
{
    int i;

    for(i = 0; i < 5; i++){ //repetir a MSG por quantidade definada
        cout << "Esta e diretiva define MSG" << "\n\n";
    }

    if(i == 5){ //se for igual a 5
        #undef MSG //remover a funcao #define
        cout << "Diretiva define MSG nao existe mais." << "\n\n";
    }

    return 0;
}
