#include <iostream>

using namespace std;

int Soma(int x){

    if(x > 0){ //se numero do argumento da funcao for maior que zero continua retornando valor
        return (x + Soma(x - 1));
    }else{
        return x;
    }
}

int main()
{
    int valor = 10;

    cout << Soma(valor) << "\n";
    return 0;
}
