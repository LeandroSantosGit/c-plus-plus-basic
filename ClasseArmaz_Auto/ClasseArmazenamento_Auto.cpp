#include<iostream>

//Variavel classe auto dentro na mesma funcao no codigo

using namespace std;

int main(){

    int variavel1 = 50;
    auto int variavel2 = 100; //variavel auto nao pode ser acessa por outra funcao no codigo

    cout << "Variavel sem auto" << variavel1 << "\n";
    cout << "Variavel2 com auto" << variavel2 << "\n";

    return 0;
}
