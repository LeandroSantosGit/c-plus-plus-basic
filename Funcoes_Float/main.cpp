#include <iostream>

using namespace std;

float Quadrado(float x){

    return x * x;
}

int main()
{
    float lado = 2.5;

    float resultado = Quadrado(lado);

    cout << resultado << "\n";

    return 0;
}
