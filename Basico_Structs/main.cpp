#include <iostream>

using namespace std;

struct Funcionarios{

        string Nome;
        int Idade;
        string Profissao;
};

int main(){

    struct Funcionarios Estagiarios = {"Leandro", 26, "Desenvolvedor"};
    struct Funcionarios TI = {"LeandroH", 30, "Manutencao"};

    cout << Estagiarios.Nome << " " << Estagiarios.Idade << " "<< Estagiarios.Profissao << "\n\n";

    cout << TI.Nome << " " << TI.Idade << " " << TI.Profissao << "\n\n";

    return 0;

}
