#include <iostream>
#include <stdio.h> //Blibioteca para o printf

//Entrada e saida de dados

using namespace std;

int main()
{
    int valor;

    cout << "Digite um valor: "; //saida de dados

    cin >> valor; //entrada de dados

    cout << "\nValor digitado: " << valor << "\n"; //saida de dados

    printf("Valor exibido com printf: %i \n", valor); //saida de dados de linguagem C

    return 0;
}
