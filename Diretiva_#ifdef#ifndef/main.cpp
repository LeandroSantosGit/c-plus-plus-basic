#include <iostream>
#define NUM 46

using namespace std;

int main()
{
    #ifdef NUM  //se definido
        cout << "O macro NUM foi declarado com valor de: " << NUM << "\n\n";
    #endif

    #undef NUM //removemos o #define

    #ifndef NUM //se nao definido
        cout << "O macro NUM nao foi declarado\n\n";
    #endif // NUM

    return 0;
}
