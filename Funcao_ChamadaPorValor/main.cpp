#include <iostream>

using namespace std;

int Quadrado(int lado){

    int resultado = lado * lado;

    return resultado;
}

int main()
{
    int valor = 40;

    cout << valor << "\n" << Quadrado(valor) << "\n" << valor;

    return 0;
}
