#include <iostream>

using namespace std;

int main()
{
    int x[] = {10, 20, 30, 40, 50}; //matriz
    int *ponteiro; //ponteiro

    for(ponteiro = x; ponteiro <= &x[2]; ponteiro++){ //comparacao e incrementar ate a posicao informada
        cout << *ponteiro << " "; //inprimir
    }

    return 0;
}
