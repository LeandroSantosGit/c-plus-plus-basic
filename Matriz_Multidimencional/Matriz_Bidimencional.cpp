#include <iostream>

using namespace std;

int main(){

    int d1, d2, d3;
    int matriz[2][2][2] = {{{10, 20}, {30, 40}}, {{50, 60},{70, 80}}};

    for(d1 = 0; d1 < 2; d1++){
        for(d2 = 0; d2 < 2; d2++){
            for(d3 = 0; d3 < 2; d3++){
                cout << matriz[d1][d2][d3] << " ";
            }
            cout << "\n";
        }
        cout << "\n";
    }

    return 0;
}
