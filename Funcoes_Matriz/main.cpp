#include <iostream>

using namespace std;

void Matriz(int M[5]){

    int i;

    for(i = 0; i < 5; i++){
        cout << M[i] << "\n";
    }
}

int main()
{
    int valores[5] = {1, 2, 3, 4, 5};

    Matriz(valores);

    return 0;
}
