## C++ B�sico

No curso foi abordado os conceitos b�sicos da linguagem: tipo de dados, vari�veis, escopo; As estruturas condicionais e repeti��o; Como trabalhar com fun��es; O funcionamento da orienta��o a objetos no C++.

* Processo de compila��o
* Tipos de dados e identificados
* Vari�veis
* Enumeradores
* Operadores
* Condicionais
* La�os
* Matrizes
* Estruturas
* Fun��es
* Ponteiros
* Entrada e Sa�da
* Orienta��o a objetos
* Pr�-processadores
* Filas e Pilhas
* DirectX