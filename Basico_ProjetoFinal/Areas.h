#ifndef AREAS_H_INCLUDED
#define AREAS_H_INCLUDED

class Areas{ //criamos uma classe e passamos os prototipos dos metodos da classe Areas.cpp

    public:   //criamos os prototipos publicos para que sejam acessado por outros arquivos
        void Quadrado();
        void Triangulo();
        void Paralelogamo();
        void Losango();
        void Retangulo();
        void Circulo();
};


#endif // AREAS_H_INCLUDED
