#include<iostream>
#include "Areas.h" //incluimos Headers da classe Area.h para implemantar os prototipos
using namespace std;

void Areas::Quadrado(){ //criamos um metodo e indentificamos qual o retorno passamos a classe dona do metodo(Areas.h) e o nome
                                                                                                        //do metodo(Quadrado)
    float Lado, Resultado; //variaveis

    cout << "\nDigite o valor de uma lado do Quandrado\n";
    cin >> Lado;

    Resultado = Lado * Lado; //efetuar o calculo do quadrado

    cout << "\nA area do Quadrado e: " << Resultado;
}

void Areas::Circulo(){

    const float PI = 3.14;
    float Raio, Resultado;

    cout << "\nDigite o valor do raio do Circulo\n";
    cin >> Raio;

    Resultado = PI * (Raio * Raio);

    cout << "\nA area do Circulo e: " << Resultado;
}

void Areas::Paralelogamo(){

    float Base, Altura, Resultado;

    cout << "\nDigite o valor da base do Parelelogamo\n";
    cin >> Base;
    cout << "\nDigite o valor da altura do Paralelogamo\n";
    cin >> Altura;

    Resultado = (Base * Altura);

    cout << "\nA area do Paralelogamo e: " << Resultado;
}

void Areas::Losango(){

    float Num1, Num2, Resultado;

    cout << "\nDigite o valor da primeira diagonal do Losango\n";
    cin >> Num1;
    cout << "\nDigite o valor da segunda diagonal de Losango\n";
    cin >> Num2;

    Resultado = (Num1 * Num2) / 2;

    cout << "\nA area do Losango e: " << Resultado;
}

void Areas::Retangulo(){

    float Base, Altura, Resultado;

    cout << "\nDigite o valor da Base do Retangulo\n";
    cin >> Base;
    cout << "\nDigite o valor da Altura do Retangulo\n";
    cin >> Altura;

    Resultado = (Base * Altura);

    cout << "\nA area do Retangulo e: " << Resultado;
}

void Areas::Triangulo(){

    float Base, Altura, Resultado;

    cout << "\nDigite o valor da base do Triangulo\n";
    cin >> Base;
    cout << "\nDigite o valor da altura do Triangulo\n";
    cin >> Altura;

    Resultado = (Base * Altura) / 2;

    cout << "\nA area do Triangulo e: " << Resultado;
}
