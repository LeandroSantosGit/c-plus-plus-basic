#include <iostream>
#include "Areas.h" //incluimos dois Headers da classe area e visual
#include "Visual.h"

using namespace std;

class Escolha:public Areas{

    private:
        int Numero; //definimos uma atributo privado

    public:
        void setNumero(int n){  //criamos um metodo sem retorno e passmos um argumento
            Inicio: //macro para receber oque o usuario digitar

                cout << "Digite a opcao: "; //informar o usuario para digitar um valor
                cin >> n; //o valor digitado pelo o usiario passamos para o argumento n

                switch(n){ //verificar o valor digitado pelo usuario
                    case 1:
                        Areas::Quadrado(); //chama o metododa classe areas
                        cout << "\n\n"; //pular duas linhas
                        goto Inicio; //voltar a execucao do programa para o inicio do switch
                        break; //frear a execucao dos outros cases
                    case 2:
                        Areas::Triangulo();
                        cout << "\n\n";
                        goto Inicio;
                        break;
                    case 3:
                        Areas::Paralelogamo();
                        cout << "\n\n";
                        goto Inicio;
                        break;
                    case 4:
                        Areas::Losango();
                        cout << "\n\n";
                        goto Inicio;
                        break;
                    case 5:
                        Areas::Retangulo();
                        cout << "\n\n";
                        goto Inicio;
                        break;
                    case 6:
                        Areas::Circulo();
                        cout << "\n\n";
                        goto Inicio;
                        break;
                    default:
                        cout << "\n\nSaindo...\n\n";
                }
        }

        int getNumero(){ //retornar o valor digitado pelo usuario passar para o atribuir a numero
            return Numero;
        }
};

int main()
{
    Visual visualizar; //criamos o objeto

    visualizar.Janla(); //chamar os metodos da classe Visual
    visualizar.Rotulos();

    Escolha opcao;

    opcao.setNumero(opcao.getNumero());//chamar o metodo setNumero e para acessar as infomacoes privado usamos o argumento getNumero

    //visualizar.Janla();

    return 0;
}
