#include <iostream>

using namespace std;

int main()
{
    int x[] = {5, 6};

    cout << " ----Soma de ponteiros---- \n";
    cout << "X = " << x << "\n";
    cout << "X + 1 = " << x + 1 << "\n";
    cout << "* X = " << *x << "\n";
    cout << "* (x+1) = " << *(x+1) << "\n";

    cout << " ----Subtracao de ponteiros---- \n\n";
    cout << "(X+1) = " << x +1 << "\n";
    cout << "(X+1) -1 = " << (x+1) -1 << "\n";
    cout << "(X+1) = " << *x + 1 << "\n";
    cout << "(X+1) -1 = " << *(x+1) -1 << "\n";


    return 0;
}
