#include <iostream>

//Matriz com 5 elementos

using namespace std;

int main(){

    int numero[5] = {1, 2, 3, 4, 5};
    int i;

    cout << "A posicao 2 tem o valor: " << numero[2] << "\n\n";

    numero[2] = 10; //alterando o valor da posicao 2

    cout << "Novo valor da posicao 2: " << numero[2] << "\n\n";

    for(i = 0; i < 5; i++){
        cout << numero[i] << "\n";
    }

    return 0;
}
