#include <iostream>

using namespace std;

void Troca(int *numero1, int *numero2){ //funcao pra trocar os valor de numero1 e numero2

    int temporario;

    temporario = *numero1; //passando o valor de numero 1 para temporario

    *numero1 = *numero2; //passando o valor de numero2 para numero1

    *numero2 = temporario; // passando o valor de temporario pra numero2
}

int main()
{
    int valor1 = 50;
    int valor2 = 20;

    cout << "Valor1: " << valor1 << "\n" << "Valor2: " << valor2 << "\n\n";

    Troca(&valor1, &valor2); //Chamando a funcao para trocar os valores

    cout << "Trocado Valor1: " << valor1 << "\n" << "Trocado Valor2: " << valor2 << "\n";

    return 0;
}
