#include <iostream>
#define NUM 101

using namespace std;

int main()
{
    #if NUM == 100
        cout << NUM << " Realmente e igual a 100.\n\n";
    #elif NUM < 100 && NUM > 50
        cout << NUM << " e menor que 100 e maior que 50.\n\n";
    #elif NUM < 50 && NUM > 0
        cout << NUM << " e maior que 0 e menor que 50.\n\n";
    #else
        cout << NUM << " este faixa de valor nao e acessivel.\n\n";
    #endif
    return 0;
}
