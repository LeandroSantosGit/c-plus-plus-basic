#include <iostream>

//fazer uma contagem decresiva de 10 a 1

using namespace std;

int main(){

    int contagem_R = 10;

    do{
        cout << contagem_R << "\n";
        contagem_R--;
    }while(contagem_R > 0);

    return 0;
}
