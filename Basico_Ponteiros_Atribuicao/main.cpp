#include <iostream>

using namespace std;

int main()
{
    int valor = 5;

    int *ponteiro1 = &valor;
    int *ponteiro2 = ponteiro1; //atribuindo o valor do ponteiro1 para ponteiro2

    cout << "Ponteiro1: " << *ponteiro1 << "\n" << "Ponteiro2: " << *ponteiro2 << "\n";
    cout << "Endereco Ponteiro1: " << ponteiro1 << "\n" << "Endereco ponteiro2: " << ponteiro2 << "\n";

    return 0;
}
