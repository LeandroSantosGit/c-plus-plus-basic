#include <iostream>

//Matriz unidimencional com 10 filmes

using namespace std;

int main(){

    string filmes[10] = {"Filme1", "Filme2", "Filme3", "Filme4", "Filme5", "Filme6", "Filme7", "Filme8", "Filme9", "Filme10"};

    int i;

    for(i = 0; i < 10; i++){
        cout << filmes[i] << "\n";
    }

    return 0;
}
