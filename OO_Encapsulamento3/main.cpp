#include <iostream>

//encapsulamento e extremamente importante termos seguranca do nosso atributos
//programa para proteger uma mensagem que so sera acessada se for digitado conforme for exigido.

using namespace std;

class Funcionarios{

    private:
        string mensagem(){
            string saudacao = "\nOla pessoal da classe 201\n\n";

            return saudacao;
        }

    public: //metodo para verificar os dados digitados
        void getClasse(string C){
            if(C == "classe201"){ //se for digitado classe201 � liberado o acesso a mensagem
                cout << mensagem();
            }else{
                cout << "\nEsta nao e classe 201\n\n";
            }
        }
};

int main()
{
    Funcionarios Ti;

    string classe;

    cout << "Digite qual a classe: ";
    cin >> classe;

    Ti.getClasse(classe);


    return 0;
}
