#include <iostream>

using namespace std;

class Funcionarios{ //crianmos a calsse

    public:
        string nome;
        int idade;
        int codigo;
};

class Analista : public Funcionarios{ //criamos a clase analista que sera filha da classe funcionarios que sera classe pai

    public:
        string setor;
        string login;
        string senha;
};

int main()
{
    Analista TI; //criamos o objeto para acessar as classes pai e filha
      //Heranca da classe Funcionarios
    TI.nome = "Leandro";
    TI.idade = 25;
    TI.codigo = 1435;
     //classe Analista
    TI.setor = "Programador";
    TI.login = "Leandro1435";
    TI.senha = "A45@9+7";

    cout << TI.nome << " " << TI.idade << " " << TI.codigo << "\n" << TI.setor << " " << TI.login << " " << TI.senha << "\n\n";

    return 0;
}
