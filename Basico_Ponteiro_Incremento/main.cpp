#include <iostream>

using namespace std;

int main()
{
    int valor[4] = {10, 20, 30, 40};
    int *ponteiro = &valor[0];

    cout << *ponteiro << "\n\n";

    *ponteiro++; //incrementando mais uma posicao da matriz

    cout << *ponteiro << "\n\n";

    *ponteiro++; //incrementando mais uma posicao da matriz

    cout << *ponteiro << "\n\n";

    return 0;
}
