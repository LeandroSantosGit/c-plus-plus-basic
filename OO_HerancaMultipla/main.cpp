#include <iostream>

using namespace std;

class Funcionarios{ //criamos uma classe

    public:
        string nome;
        int idade;
        int codigo;
};

class Escolaridade{ //criamos uma classe

    public:
        string formacao;
        int quant_cursos;

};

class Analista : public Funcionarios, public Escolaridade{
    //criamos a classe que te classe Funcionario primeiro pai e Escolaridade segundo pai

    public:
        string setor;
        string login;
        string senha;
};

int main()
{
    Analista TI; //criamos o objeto TI
    //Herancao de Funcionario
    TI.nome = "Leandro";
    TI.idade = 25;
    TI.codigo = 1435;
     //Atributos classe Analista
    TI.setor = "Seguranca";
    TI.login = "Admim";
    TI.senha = "6al@5#.3L";
     //Heranca de Escolaridade
    TI.formacao = "Sistema de Informacao";
    TI.quant_cursos = 11;

    cout << "Funcionario: " << TI.nome << "\n" << "Idade: " << TI.idade << "\n" << "Codigo: " << TI.codigo << "\n";
    cout << "Setor: " << TI.setor << "\n" << "Login: " << TI.login << "\n" << "Senha: " << TI.senha << "\n";
    cout << "Formacao: " << TI.formacao << "\n" << "Quantos Cursos: " << TI.quant_cursos << "\n";

    return 0;
}
