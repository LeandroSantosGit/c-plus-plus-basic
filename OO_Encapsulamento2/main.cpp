#include <iostream>

using namespace std;

class Funcionarios{

    private:
        string nome;

    public:
        void setNome(string n){
            if(n != "Leandro"){ //verificar se valor � diferente de Leandro e se for passara para atributo nome
                nome = n;
                cout << "Esse nome pode ser usado\n\n";
            }else{
                cout << "Esse nome nao pode ser usado\n\n";
            }
        }

        string getNome(){
            return nome;
        }
};

int main()
{
    Funcionarios TI;

    string Nome;

    cout << "Digite o nome: ";
    cin >> Nome; //para o usuario digitar o nome

    TI.setNome(Nome);

    cout << TI.getNome() << "\n\n";

    return 0;
}
