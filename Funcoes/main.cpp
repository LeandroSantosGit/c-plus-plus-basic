#include <iostream>

using namespace std;

void Soma(){

    int numero1 = 10;
    int numero2 = 20;

    int resultado = numero1 + numero2;

    cout << "Resultado da soma: " << resultado << "\n";

}

int main()
{
    Soma();

    return 0;
}
