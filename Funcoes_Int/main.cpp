#include <iostream>

using namespace std;

int Modulo(int x, int y){

    return x % y;
}

int main()
{
    int valor1 = 25;
    int valor2 = 10;

    int resultado = Modulo(valor1, valor2);

    cout << "Resultado do resto da divisao: " << resultado << "\n";

    return 0;
}
