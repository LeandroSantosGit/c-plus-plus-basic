#include <iostream>

using namespace std;

int main()
{
    int valor = 5;

    int *ponteiro = &valor;

    cout << "Valor no ponteiro: " << *ponteiro << "\n";

    cout << "Endereco de memoria do ponteiro: " << ponteiro << "\n";

    return 0;
}
