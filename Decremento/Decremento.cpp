#include <iostream>

using namespace std;

int main(){

    int x = 1;
    int y = 1;

    x--;
    --y;

    cout << "Decremento X: " << x << "\n" << "Decremento Y: " << y << "\n";

    return 0;

}
