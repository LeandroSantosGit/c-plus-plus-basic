#include<iostream>

using namespace std;

class Funcionario{ //criammos classe

    private: //definimos que os atributos seram privados
        string nome;

    public: //criamos dois metodos como publicos
        void setNome(string n){ //metodo set ira tribuir um valor ao atributo nome que nao pode ser acessada diretamente
            nome = n;
        }

        string getNome(){ //metodo get ira retornar contido no atributo nome
            return nome;
        }
};

int main(){

    Funcionario TI;

    string Nome = "Leandro Santos";

    TI.setNome(Nome);

    cout << TI.getNome();

    return 0;
}
