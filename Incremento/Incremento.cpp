#include <iostream>

using namespace std;

int main(){

    int x = 1;
    int y = 1;

    x++;
    ++y;

    cout << "Incremento de X: " << x << "\n" << "Incremento de Y: " << y << "\n";

    return 0;
}
