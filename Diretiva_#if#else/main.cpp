#include <iostream>
#define NUM 100

using namespace std;

int main()
{
    #if NUM == 100
        cout << NUM << " " << "Realmente e o valor de NUM.\n\n";
    #else
        cout << NUM << " " << "Nao e igual a 100.\n\n";
    #endif // NUM

    return 0;
}
