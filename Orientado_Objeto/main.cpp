#include <iostream>

using namespace std;

class Cachorro{ //Criar classe cachorro com os atributos principais

    public:
        string nome; //definimos os atributos que fornecem caracteristicas da classe
        int idade;
        string raca;

};

int main()
{
    Cachorro dog; //Crianmos o objeto dog uma instancia da classe cachorro

    dog.nome = "Toto"; //usamos o objeto com um ponto para acessar os atributos da classe e passamos os valor
    dog.idade = 5;
    dog.raca = "Vira Lata";

    cout << "O cachorro se chama " << dog.nome << " e tem " << dog.idade << " anos de idade sendo da raca " << dog.raca << "\n\n";

    Cachorro dog2; //Crianmos o objeto dog uma instancia da classe cachorro

    dog2.nome = "Leao"; //usamos o objeto com um ponto para acessar os atributos da classe e passamos os valor
    dog2.idade = 2;
    dog2.raca = "Pitbull";
                    //imprimir o dados para o usuario ultilizando o objeto com um ponto para acessar os atributos com seus valores
    cout << "O cachorro se chama " << dog2.nome << " e tem " << dog2.idade << " anos de idade sendo da raca " << dog2.raca << "\n\n";

    return 0;
}
