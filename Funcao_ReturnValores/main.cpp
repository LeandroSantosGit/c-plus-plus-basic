#include <iostream>

using namespace std;

int Multiplicacao(int x, int y){

    return x * y;
}

int main()
{
    int numero1 = 5;
    int numero2 = 10;

    int resultado = Multiplicacao(numero1, numero2);

    cout << "Numero1: " << numero1 << "\n" << "Numero2: " << numero2 << "\n\n";
    cout << "Resultado da Multiplicacao: " << resultado << "\n";

    return 0;
}
