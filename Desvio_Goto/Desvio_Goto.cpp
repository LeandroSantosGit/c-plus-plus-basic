#include <iostream>

//goto repete uma determinada funcao

using namespace std;

int main(){

    int sorvete = 2;

    Tomar_sorvete:
        sorvete--;
        cout << "Menos um sorvete" << sorvete << "\n";

        if(sorvete > 0){
            goto Tomar_sorvete;
        }

    return 0;
}
