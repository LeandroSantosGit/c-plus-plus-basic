#include <iostream>

//program contar de 0 a 50, mais quero que pare em 25

using namespace std;

int main(){

    int contagem = 0;

    while(contagem < 50){
        contagem++;
        cout << contagem << "\n";
        if(contagem == 25){
            break;
        }
    }

    return 0;
}
