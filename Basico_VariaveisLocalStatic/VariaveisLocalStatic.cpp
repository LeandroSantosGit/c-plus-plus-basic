#include<iostream>

//variavel local static mantem seu valor a cada chamada

using namespace std;

void Soma();

int main(){

    Soma();
    Soma();
    Soma();
    Soma();

    return 0;

}

void Soma(){

    static int numero = 0;

    numero = numero + 1;

    cout << numero << "\n";
}
