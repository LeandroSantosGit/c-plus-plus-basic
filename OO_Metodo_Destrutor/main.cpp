#include <iostream>

using namespace std;

class Triangulo{

    public:
        Triangulo(){
            cout << "Metodo construtor\n\n";
        }

        ~Triangulo(){
            cout << "Metodo destrutor. Objeto morreu\n";
        }

};

int main()
{
    Triangulo t;

    return 0;
}
