#include <iostream>

using namespace std;

int main(int argc, char *argv)
{
    cout << "Temos como argumento do ARGC: " << argc << "\n";

    cout << "Temos como argumento do ARGV: " << argv << "\n";

    return 0;
}

//argc � um inteiro que informa quantos argumento foram passados e seu valor minimo 1
//argv ponteiro para uma matriz de string e casa string � um argumento
