#include <iostream>

using namespace std;

class Cachorro{ //criamos uma classe

    public:  //atributos serao publicos
        string racaCachorro; //adcionamos os principais atributos de cachorro
        string nomeCahorro;
        int idadeCachorro;

        void Latir(){ // criamos os metodos que sao as acoes de cachorro

            cout << "Au, Au, Au" << "\n\n"; // imprimir o latido
        }

        void ExibirInformacao(string raca, string nome, int idade){  //metodo pra exibir as imformacoes do cachorro
            cout << "O cachorro de nome " << nome << " da raca " << raca << " tem " << idade << " anos de idade." << "\n";
        }
};

int main()
{
    Cachorro dog; //criamos o objeto da classe cachorro

    dog.racaCachorro = "Vira Lata"; //definir os valores dos atributos
    dog.nomeCahorro = "Toto";
    dog.idadeCachorro = 3;
            //chamar os metodos de imfomacoes e latir
    dog.ExibirInformacao(dog.racaCachorro, dog.nomeCahorro, dog.idadeCachorro); //passar os argumetos para serem imprimidos
    dog.Latir();

    Cachorro cao;

    cao.racaCachorro = "Pitbull";
    cao.nomeCahorro = "Negao";
    cao.idadeCachorro = 5;

    cao.ExibirInformacao(cao.racaCachorro, cao.nomeCahorro, cao.idadeCachorro);
    cao.Latir();

    return 0;
}
