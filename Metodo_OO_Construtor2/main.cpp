#include <iostream>

using namespace std;

class Numeros{

    private:
        int x, y, z;

    public:
        Numeros(int x1 = 50, int y1 = 60, int z1 = 70){ //metodo construtor com argumentos e valores

            x = x1;
            y = y1;
            z = z1;

            cout << "X: " << x << "\n" << "Y: " << y << "\n" << "Z: " << z << "\n";
        }
};

int main()
{
    Numeros exemplo;

    return 0;
}
