#include <iostream>

using namespace std;

class Cachorro{ //criamos a classe

    public:
        string nome; //criamos os principais atributos de um cachorro
        int idade;
        string raca;

        void Latir(){ //criamos metodos que � uma acao de cachorro

            cout << "Au, Au, Au" << "\n\n";
        }
};

int main()
{
    Cachorro dog; //criamos o objeto

    dog.Latir(); //usamos o objeto para executar o metodo

    return 0;
}
