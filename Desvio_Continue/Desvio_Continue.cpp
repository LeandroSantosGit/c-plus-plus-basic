#include <iostream>

//contagem de 1 a 50, continuar em 25 ate 40

using namespace std;

int main(){

    int contagem = 0;

    while(contagem < 50){
        contagem++;
        cout << contagem << "\n";
        if(contagem == 25){
            cout << "Vamos continuar ate o numero 40 \n";
            continue;
        }
        if(contagem == 40){
            cout << "Paramos em : " << contagem << "\n";
            break;
        }
    }

    return 0;
}
