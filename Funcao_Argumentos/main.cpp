#include <iostream>

using namespace std;

void Soma(int num1, int num2){ //funcao com passando argumentos entre os parenteses

    int resultado = num1 + num2;

    cout << "O resultado da soma: " << resultado << "\n";
}

int main()
{
    int valor1 = 10;
    int valor2 = 25;

    Soma(valor1, valor2);

    return 0;
}
