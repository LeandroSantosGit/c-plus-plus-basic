#include <iostream>

//Tabela matriz para armazenar notas 3 notas de 3 alunos

using namespace std;

int main(){

    int linha, coluna;
    float notas[3][3] = {

    8.2, 9.1, 7.5,
    5.5, 5.2, 6.6,
    6.5, 7.4, 6.2
    };

    for(linha = 0; linha < 3; linha++){
        for(coluna = 0; coluna < 3; coluna++){
            cout << notas[linha][coluna] << " ";
        }
        cout << "\n";
    }

    return 0;
}
