#include<iostream>

//Variavel global static mantem seu valor a cada chamada e pode ser chamada em qualquer funcao do bloco de codigo

using namespace std;

static int numero = 10;

void Soma();

int main(){

    Soma();
    Soma();
    Soma();
    Soma();
}

void Soma(){

    numero = numero + 1;

    cout << numero << "\n";
}
