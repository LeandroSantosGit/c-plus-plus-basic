#include <iostream>

using namespace std;

int main()
{
    int valor[4] = {10, 20, 30, 40};
    int *ponteiro = &valor[3];

    cout << *ponteiro << "\n";

    *ponteiro--; ////decrementando mais uma posicao da matriz

    cout << *ponteiro << "\n";

    *ponteiro--; //decrementando mais uma posicao da matriz

    cout << *ponteiro << "\n";

    return 0;
}
